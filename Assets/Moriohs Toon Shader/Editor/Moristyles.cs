﻿#region

using System;
using UnityEditor;
using UnityEngine;

#endregion

// help link https://docs.unity3d.com/ScriptReference/EditorStyles.html
// ---DISCLAIMER--- THIS CODE IS BASED OFF OF "SYNQARK"'s ARKTOON-SHADERS AND "XIEXE"'s UNITY-SHADERS. FOR MORE INFORMATION PLEASE REFER TO THE ORIGINAL BASE WRITER "https://github.com/synqark", "https://github.com/synqark/Arktoon-Shaders" or "https://github.com/Xiexe", "https://github.com/Xiexe/Xiexes-Unity-Shaders"

public class Moristyles : MonoBehaviour
{
    public const string ver = "<color=#00ffffff> ✿ {  </color>Mori's Toon v." + "<color=#FFFF00ff> 1.7.1</color> | <color=#00ff00ff>Git-Rev.2</color>" + "<color=#00ffffff>  } ✿ </color>";

    //Shuriken Toggle and variants of it
    private static Rect DrawShuriken(string title, Vector2 contentOffset, int HeaderHeight)
    {
        var style = new GUIStyle("ShurikenModuleTitle")
        {
            font = new GUIStyle(EditorStyles.boldLabel).font,
            border = new RectOffset(15, 7, 4, 4),
            fixedHeight = HeaderHeight,
            contentOffset = contentOffset
        };
        var rect = GUILayoutUtility.GetRect(16f, HeaderHeight, style);
        GUI.Box(rect, title, style);
        return rect;
    }

    public const int shurikenIndent = 11;
    public static bool ShurikenFoldout(string title, bool display, int indent)
    {
        var rect = DrawShuriken(title, new Vector2(20+indent, -2f), 22);
        var e = Event.current;
        var toggleRect = new Rect(rect.x + 4+indent, rect.y + 2f, 13f, 13f);
        if (e.type == EventType.Repaint) EditorStyles.foldout.Draw(toggleRect, false, false, display, false);
        if (e.type == EventType.MouseDown && rect.Contains(e.mousePosition))
        {
            display = !display;
            e.Use();
        }
        return display;
    }
    //Shuriken Toggle and variants of it end
    
    
    //Title Shuriken for text only
    private static void DrawShurikenCenteredTitle(string title, Vector2 contentOffset, int HeaderHeight)
    {
        var style = new GUIStyle("ShurikenModuleTitle")
        {
            font = new GUIStyle(EditorStyles.boldLabel).font,
            fontSize = 12,
            border = new RectOffset(15, 7, 4, 4),
            fixedHeight = HeaderHeight,
            contentOffset = contentOffset,
            alignment = TextAnchor.MiddleCenter
        };
        var rect = GUILayoutUtility.GetRect(16f, HeaderHeight, style);

        GUI.Box(rect, title, style);
    }
    
    public static void ShurikenHeaderCentered(string title)
    {
        DrawShurikenCenteredTitle(title, new Vector2(0f, -2f), 28);
    }
    //Title Shuriken for text only end
    
    
    //parting lines
    private static GUIStyle _LineStyle;
    private static GUIStyle LineStyle => _LineStyle ?? (_LineStyle = new GUIStyle
    {
        normal =
        {
            background = EditorGUIUtility.whiteTexture
        },
        stretchWidth = true
    });
    
    private static void GUILine(Color color, float height = 0f)
    {
        Rect position = GUILayoutUtility.GetRect(0f, float.MaxValue, height, height, LineStyle);

        if (Event.current.type == EventType.Repaint)
        {
            Color orgColor = GUI.color;
            GUI.color = orgColor * color;
            LineStyle.Draw(position, false, false, false, false);
            GUI.color = orgColor;
        }
    }
    
    public static void PartingLine()
    {
        GUILayout.Space(5);
        GUILine(new Color(.75f, .75f, .75f), 1.5f);
        GUILayout.Space(5);
    }
    //parting lines end
    

    //exrta buttons
    private static void gitlabversioncheckbutton(int Width, int Height)
    {
        if (GUILayout.Button("Check Version", GUILayout.Width(Width), GUILayout.Height(Height))) Application.OpenURL("https://gitlab.com/xMorioh/moriohs-toon-shader");
    }

    private static void gitupdatebutton(int Width, int Height)
    {
        if (GUILayout.Button("Update", GUILayout.Width(Width), GUILayout.Height(Height))) Application.OpenURL("https://gitlab.com/xMorioh/moriohs-toon-shader/-/archive/master/moriohs-toon-shader-master.zip");
    }

    private static void manualbutton(int Width, int Height)
    {
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        if (GUILayout.Button("Show Manual", GUILayout.Width(Width), GUILayout.Height(Height))) Application.OpenURL("https://gitlab.com/xMorioh/moriohs-toon-shader/-/wikis/Mori's-Toon-Shader-Manual");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
    }

    public static void DrawButtons()
    {
        PartingLine();
        EditorGUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        gitlabversioncheckbutton(100, 30);
        gitupdatebutton(100, 30);
        GUILayout.FlexibleSpace();
        EditorGUILayout.EndHorizontal();
        manualbutton(100, 30);
    }
    //exrta buttons end

    // Simple auto-laid out information box, uses materialproperty display name as text | Ported from Kaj's Editor
    public class HelpBoxDrawer : MaterialPropertyDrawer
    {
        private readonly MessageType type;

        public HelpBoxDrawer()
        {
            type = MessageType.Info;
        }

        public HelpBoxDrawer(float f)
        {
            type = (MessageType) (int) f;
        }

        public override void OnGUI(Rect position, MaterialProperty prop, string label, MaterialEditor editor)
        {
            EditorGUILayout.HelpBox(label, type);
        }

        public override float GetPropertyHeight(MaterialProperty prop, string label, MaterialEditor editor)
        {
            return -4f; // Remove the extra drawer padding + helpbox extra padding
        }
    }
    // Simple auto-laid out information box, uses materialproperty display name as text | Ported from Kaj's Editor end
}