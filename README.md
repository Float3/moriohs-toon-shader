# Morioh's Toon Shader
# [Can't find the Download link? Click here](https://gitlab.com/xMorioh/moriohs-toon-shader/-/archive/master/moriohs-toon-shader-master.zip)
# [You dont know how to use the Shader ? Here is a manual you can read ](https://gitlab.com/xMorioh/moriohs-toon-shader/-/wikis/Mori's-Toon-Shader-Manual)
# [For a better Overview click here](https://xmorioh.gitlab.io/Moriohs%20Toon%20Shader.html)


**About this Shader**

The Goal was to make a Toon/Master Shader that not just correctly respects the entirety of Unity's Lighting System in a NPR Workflow but would also include several PBR approaches for different features.
<br>
A good amount of the visual Lighting knowledge came from the [VRChat Shader Development discord group](https://discord.gg/XyU4MJv).
<br>
It's made with the end user in mind, meaning that by default only the basic options are visible in the Inspector and all the Advanced stuff is hidden unless you really need them. This can be toggled at the top of the Inspector and keeps all the bloat away for beginners.
<br>
Also the goal was to make a Master Shader like this that would be fully supported by the [Amplify Shader Asset](https://assetstore.unity.com/packages/tools/visual-scripting/amplify-shader-editor-68570) so other developers can use it as a Template.
<br>
In the end performance an visuals are the two things that matter the most and you will get the best of both worlds using this Shader. Usually making a Surface Shader like this via Amplify hurts performance pretty badly but thankfully [Kaj](https://github.com/DarthShader/Kaj-Unity-Shaders) provided me his Shader Optimizer which improves performance to the degree of a self written Shader and even better performance than that. I've consistently got 50-55% improvements in rendertimes using the optimizer and many other devs use it too.

**Features included**:
* Texture Saturation
* Pixel, Vertex and Indirect Light Shadow Ramp Offset, Opacity and Color option
* Dithering with custom Texture support
* Step Based Cel Shading
* Occlusion Map support
* View based Shadow and Emissive Rim
* Normal and detailed Normal Map support with a masking Function
* Basic Emission with a Light dampener
* PBR, Toon and Anisotropic(GGX and Blinn) Specular Highlights
* Four Layer Matcap with a Mask and other options
* Rim Light with several options
* Subsurface Scattering with several options
* PBR Reflections with Fallback to a selectable Baked Cubemap
* Two Emission Scroll systems with [llealloo's AudioLink](https://github.com/llealloo/vrc-udon-audio-link)
* Flipbook
* Main Texture HUE Shift Function
* Dissolve and Materialize Function
* A whole Set of Render options for Blend, Stencil and Depth options that are available in Unity


**Versions**:
* Basic  = A bare bone default version which is trimmed to only include necessary settings and Functions for Lightweight rendering, easy setup and best Performance.
* Advanced  = This Version includes all Functions and Features listed in [Features included] for the best possible looking Toon or PBR outcome that you want to achieve.
* You can switch between the two versions via Shader Feature Toggle


**Variants**:
* Opaque
* Cutout
* Outlined
* Transparent

<br>
//Disclaimer

This Shader is entirely made with the Amplify Shader Editor Asset.
<br>
This Shader does not support Deffered Rendering nor Lightmap Baking and should not be used for Worlds but rather for Avatars specifically.

Tested with Unity 2019.4.20f1, 2020.3.11f1, 2021.1.10f1

Last Words:

I want to thank [Xiexe#0001](https://github.com/Xiexe) heavily for inspiring me to create my own Toon Shader ! You should definitely check out his Stuff.
<br>
And a special thanks to Quinix#0001 to help me out with some lighting trouble i've had in the beginning of this Project since i'd probably have dropped it at some point because of it!
<br>
Also i want to thank [ACIIL#4694](https://github.com/ACIIL) for providing me with his approach for a really good Cubemap Fallback and general Light limiter.
<br>
The same goes for [Lyuma#0781](https://github.com/lyuma) for helping me out with some Surface Shader workarounds for specific things.
<br>
And for [Kaj](https://github.com/DarthShader/Kaj-Unity-Shaders) for providing me his Optimizer !
<br>
For support or questions, contact me on Discord. [Morioh#0041](https://discord.com/users/275734177653981186)
<br>
<br>
**Forks**:
<br>
[Rithrin](https://gitlab.com/rithrin1/moriohs-toon-shader-Rithrin-Fork)
<br>
<br>

**Video**
<p>
<div align="center"><a href="https://youtu.be/tInhZMzxmRo"><img alt="MoriohsToonShaderInspector" src="https://gitlab.com/xMorioh/moriohs-toon-shader/-/wikis/uploads/8f87824223425a95e1597913f48247f0/MoriToonShowcaseYouTube.png" width="834"></a></div>
</p>
<br>

**Pictures**
<p>
<div align="center"><img src="https://xmorioh.gitlab.io/img/MoriohsToonShaderInspector.png" alt="MoriohsToonShaderInspector" width="556"/></div>

<div align="center"><img src="https://xmorioh.gitlab.io/img/MoriohsToonShaderExample1.jpg" alt="MoriohsToonShaderExample1" width="834"/></div>

<div align="center"><img src="https://xmorioh.gitlab.io/img/MoriohsToonShaderExample2.jpg" alt="MoriohsToonShaderExample2" width="834"/></div>

<div align="center"><img src="https://xmorioh.gitlab.io/img/MoriohsToonShaderExample3.jpg" alt="MoriohsToonShaderExample3" width="834"/></div>
</p>
